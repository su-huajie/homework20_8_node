/**
 * 数据库相关操作
 * 
 */

let model = {

    unusQuery: function (sql, value, res) {
        let mysql = require("mysql");

        let connection = mysql.createConnection({ host: 'localhost', user: 'root', password: 'root', database: 'yiqing', port: '3306' });

        connection.connect();

        connection.query(sql, value, function (err, result) {

            if (err) {
                console.log(err.message)
            }
            console.log(result);
            //对数据进行处理
            //构建html 展示给用户
            //
            let html = '<div>';
            for (let val in result) {
                console.log(result[val].city);
                console.log(result[val].nums);
                html += '<div class="city">' + result[val].city + '</div>';
                html += '<div class="nums">' + result[val].nums + '</div>';

            }
            html += "</div>"

            res.end(html);


        })

        connection.end();
    },

    query: function (sql, value) {


        let promise=new Promise(function (resovle, reject) {

            let mysql = require("mysql");

            let connection = mysql.createConnection({ host: 'localhost', user: 'root', password: 'root', database: 'yiqing', port: '3306' });

            connection.connect();

            connection.query(sql, value, function (err, result) {

                if (err) {
                    reject(err.message)
                }else{
                    setTimeout(function(){//模拟数据库的慢查询
                        resovle(result);
                    },5000)                   
                }
                

            })

            connection.end();

        })

        return promise;


    }



}



module.exports = model
