let http = require("http");
let fs = require("fs");
let nunjucks = require("numjucks");

http.createServer(async function (req,res) {
    let url = req.url;
    //静态资源，还有不完善的地方，一些文件类似是用户不能访问，exe bat 是不能访问的；
    //比方我们的localhost：8080，我们要对静态url的文件格式进行检测
    if (url.lastIndexOf(".">-1)) {
        let path = '.'+url;//
        console.log(path);
        fs.exists(path,function (result) {
            if (!result) {
                path = './home/imgs/404.png';
            }
            fs.readFile(path,function (err,data) {
                if (err) {
                    res.end(err.message);
                }else{
                    res.end(err.message);
                    res.end();
                }
            })
        })
    }else{
        res.setHeader("Content-type","text/html;charset=utf8");
        //动态资源。动态资源正常来说就是要找数据
        //首先是定位到我们的controller。通过url的参数c 比方说
        //我们要获取到参数c从而去定位我们的controller（controler是指挥者）
        let url = req.url;
        //是url/path？c=index&a=index 我们需要的是query（c= index&a=index）
        let query =url.split('?')[1];//取出c=index&a=index
        let queerArry = query.split('&');//得到数据【c=index,a=index】;
        let needArr =[];
        for(let val in queerArry){
            let temArr = queerArry[val].split('.');
            needArr[temArr[0]] = temArr[1];
        }
        req.query = needArr;//我们后续可能还要从这个url中提取参数，先赋值到req这个对象上
        let controllerName = needArr['c'];
        //controller是mvc的指挥者，执行动态网页的逻辑所在，所以先查找contrlller
        //用class 去替换json 后因为这个class 的命令规则以（IndexController）为例。需把首字母大写
        controllerName = controllerName.replace(controllerName[0],controllerName[0].toUpperCase())+'Controller'
        console.log(controllerName);
        let path = './home/controller/'+controllerName;//controller的路径地址
        let controller = require(path);//引入controller ，变量controller 就是引入的Index（jason格式）
        let action = needArr['a'];//要执行的函数，action是个变量
        let obj = new controller(req,res,nunjucks);//创建了对象
        let result =await obj[action]();
        res.write(result);
        res.end()

    }


}).listen(8080);
async function handle(obj,action) {
    let result = await obj[action]();
    return result;
}