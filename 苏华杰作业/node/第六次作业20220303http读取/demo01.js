/**
 * 作业:
 * 
 * 创建一个web服务器
 * 
 * 根据不同的url 去读取页面,给前端渲染
 * 
 */

 let http =require("http");
 let fs =require("fs");
 let server =http.createServer();
 server.on('request',(req,res)=>{
     let url = req.url;
     if (url.indexOf("page")!=-1) {
         let page = url.split("?")[1].split('=')[1]
         let bf = fs.readFileSync('./html/'+page+'.txt');
         res.write(bf);
     }
     res.end();//标识这个require请求结束
 })
 server.listen(80);//端口80web服务器的默认端口，在浏览器上会省率
