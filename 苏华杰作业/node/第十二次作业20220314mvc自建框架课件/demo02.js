let http = require("http");
let fs = require("fs");

http.createServer(function (req.res) {
    let url =req.url;
    //静态资源
    if (url.lastIndexOf(".")>-1) {
        let path = '.'+url;
        console.log(path);
        //web服务器上的代码，一定要异步的
        fs.exists(path,function (result) {
            if (!result) {
                path ='./home/imgs/404.png'
            }
            fs.readFile(path,function (err,data) {
                if (err) {
                    res.end(err.message);
                }else{
                    res.write(data);
                    res.end();
                }
            })
        })
    }else{
        //动态资源，动态资源正常来说就是要找数据库
        res.end();
    }
}).listen(8080);