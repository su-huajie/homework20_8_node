function demoPromise() {
    let promise = new Promise(function (resolve,reject) {
        setTimeout(function () {
            resolve('你滚');
            console.log(promise);
        },2000)

    })
    return promise;
}

async function demoAwaitAndPromiseAndAsync() {
    let result = await demoPromise();
    console.log('上');
    console.log(result);
    console.log('下');
}