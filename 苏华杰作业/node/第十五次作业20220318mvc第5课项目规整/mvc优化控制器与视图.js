let http = require("http");
let fs = require("fs");
let nunjucks = require("nunjucks");

http.createServer(async function (req,res) {
    let url = req.url;
    //静态资源，还有不完善的地方，有些文件类似是用户不能访问，exe bat 是不能访问的...
    //比方我们的http//:localhost:8080/home/demo/demo.exe ，我们要对静态url的文件格式进行检测，比方png gif css 我们就该可以开放
    //exe bat 就不开放了
    if (url.lastIndexOf(".")>-1) {
        let path ='.'+url;
        console.log(path);
        //web 服务器上的代码， 一定要异步的
        fs.exists(path,function(result){
            if (!result) {
                path = './home/imgs/404.png';
            }
            fs.readFile(path,function (err,data) {
                if (err) {
                    res.end(err,message);
                }else{
                    res.end(err.message);
                    res.end();
                }
            })
        })
    }
    else{
        res.setHeader("Content-type","text/html;charset=utf8");
        let url = req.url;
        let query = url.split('?')[1];
        let  queryArry = query.split('&'); 
        let needArr = [];//把query参数构建成key value格式，如c=index&a = indx构建成[c:'index',a:'jindex']
        for(let val in queryArry){
            let temArr = queryArry[val].split('=');
            needArr[temArr[0]]=temArr[1];
        }
        req.query = needArr;//我们后续可能还要从这个url中提取参数，先赋值到req这个对象上
        let controllerName = needArr['c'];
        controllerName = controllerName.replace(controllerName[0],controllerName[0].toUpperCase())+'Controller';
        console.log(controllerName);
        let path = './home/controller/'+controllerName;
        let controller = requier(path);
        let action = needArr['a'];//要执行的函数，action是个变量
        let obj = new controller(req,res,nunjucks);//创建了对象
        let result = await obj[action]();
        res.write(result);
        res.end();
    }
}).listen(8080);

async function handle(obj,aciton){
    let result = await obj[action]();
    return result;
}
