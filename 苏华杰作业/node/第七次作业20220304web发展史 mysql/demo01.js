
/**
 * 作业布置
 * 
 * 优化 http 处理不同静态文件 css js image,放在不同的目录 css 放于 css 目录下
 * 
 * js放于 js 目录下，image 放于 image 下
 * 
 * 
 * mvc 是一种思想,一种设计模式
 * 
 * 
 *     express koa koa2 
 * 
 * 
 */


let http = require("http");
let fs = require("fs");
let server = http.createServer();

server.on('request',function (req,res) {
    let url =req.url;
    console.log(url);
    //说明是以.为结束，我们自己定位为静态资源
    if (url.lastIndexOf(".")>-1) {
        let ziyuan = '.'+url;
        if (fs.existsSync(ziyuan)) {
            let buffer = fs.readFileSync(ziyuan);
            res.write(buffer);
        }
    }
    res.end("ok")
});
server.listen(8080,function () {
    console.log("服务已启动：http://127.0.0.1:3000");
});
